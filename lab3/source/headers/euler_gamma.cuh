//
// Created by wojciech on 29.05.2021.
//

#ifndef PRIR_EULER_GAMMA_CUH
#define PRIR_EULER_GAMMA_CUH

#define THREADS 512

__host__ double function_euler_cpu(int k, int n) {
    return 1.0/k;
}

__device__ double function_euler_gpu(int k, int n) {
    return 1.0/k;
}

__global__ void sum_euler(int n, double *solution) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if(index < n) solution[index] = function_euler_gpu(index + 1, n);
}

double seq_euler_solve(int n) {
    double solution = 0;
    for (int i = 1; i <= n; i++) {
        solution += function_euler_cpu(i, n);
    }
    return solution - log(n);
}

double cuda_euler_solve(int n) {
    double *solution, *solution_gpu;
    int size = n * sizeof(double);
    cudaMalloc((void **)&solution_gpu, size);
    solution = (double *)malloc(size);
    sum_euler<<<(n + THREADS - 1) / THREADS, THREADS>>>(n, solution_gpu);
    cudaMemcpy(solution, solution_gpu, size, cudaMemcpyDeviceToHost);
    double reduction = 0;
    for (int i = 0; i < n; i++) {
        reduction += solution[i];
    }
    free(solution);
    cudaFree(solution_gpu);
    return reduction - log(n);
}

long seq_euler_print(int n) {
    auto time_start = std::chrono::steady_clock::now();
    double solution = seq_euler_solve(n);
    auto time_end = std::chrono::steady_clock::now();
    printf(
            "Wartosc sumy: %.20f, Blad: %.5f%\n",
            solution, (abs(0.5772156649 - solution) / 0.5772156649) * 100
    );
    return std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_start).count();
}

long cuda_euler_print(int n) {
    auto time_start = std::chrono::steady_clock::now();
    double solution = cuda_euler_solve(n);
    auto time_end = std::chrono::steady_clock::now();
    printf(
            "Wartosc sumy: %.20f, Blad: %.5f%\n",
            solution, (abs(0.5772156649 - solution) / 0.5772156649) * 100
    );
    return std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_start).count();
}

#endif //PRIR_EULER_GAMMA_CUH
