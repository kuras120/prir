//Zrownoleglenie - kilka blokow - kilka watkow + rownowazenie
#include<stdio.h>
#include <time.h>
#define N 30
#define M 8 //Watki na blok

__global__ void add(int *a, int *b, int *c, int n) {
    printf("%d %d %d\n", threadIdx.x, blockIdx.x, blockDim.x);
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    printf("%d\n", index);
    if(index<n)
        c[index] = a[index] + b[index];
}

void not_divided() {
    printf("Number of blocks: %d\n", N);
    int *a, *b, *c; // host copies of a, b, c
    int *d_a, *d_b, *d_c; // device copies of a, b, c
    int size = N * sizeof(int);
    int i;
    srand(time(NULL));
    // Allocate space for device copies of a, b, c
    cudaMalloc((void **)&d_a, size);
    cudaMalloc((void **)&d_b, size);
    cudaMalloc((void **)&d_c, size);
    // Alloc space for host copies of a, b, c and setup input values
    a = (int *)malloc(size); random(a, N);
    b = (int *)malloc(size); random(b, N);
    c = (int *)malloc(size);
    // Copy inputs to device
    cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);
    // Launch add() kernel on GPU
    add<<<(N+M-1)/M,M>>>(d_a, d_b, d_c, N);
    // Copy result back to host
    cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);

    for(i=0;i<N;i++) {
        printf("a[%d](%d) + b[%d](%d) = c[%d](%d)\n",i,a[i],i,b[i],i,c[i]);
    }
    // Cleanup
    //printf("%d+%d=%d\n",a,b,c);
    free(a); free(b); free(c);
    cudaFree(d_a); cudaFree(d_b); cudaFree(d_c);
}
