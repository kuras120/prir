//
// Created by wojciech on 24.05.2021.
//

#ifndef PRIR_SEC_INTEGRAL_H
#define PRIR_SEC_INTEGRAL_H

#include <cstdio>
#include <chrono>
#include <unistd.h>

#define THREADS 512

__host__ double function_pi_cpu(double x) {
    return abs(1 / (1 + pow(x, 2)));
}

__device__ double function_pi_gpu(double x) {
    return abs(1 / (1 + pow(x, 2)));
}

__global__ void sum_pi(double start, double interval, double *solution, int sections) {
//    printf("%d %d %d\n", threadIdx.x, blockIdx.x, blockDim.x);
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if(index < sections) {
        double tempStart = start + (interval * index);
        double tempEnd = start + (interval * (index + 1));
        solution[index] = function_pi_gpu(tempStart) + function_pi_gpu(tempEnd);
//        printf("thread: %d index: %d solution: %f\n", threadIdx.x, index, solution[index]);
    }
}

double seq_pi_solve(double start, double end, int sections) {
    double solution = 0;
    double interval = (end - start) / sections;
    double tempStart = start, tempEnd;
    for (int i = 1; i <= sections; i ++) {
        tempEnd = start + (interval * i);
//        printf("index: %d solution: %f\n", i - 1, function_cpu(tempStart) + function_pi_cpu(tempEnd));
        solution += function_pi_cpu(tempStart) + function_pi_cpu(tempEnd);
        tempStart = tempEnd;
    }
    return 4 * solution * interval * 0.5;
}

double cuda_pi_solve(double start, double end, int sections) {
    double *solution, *solution_gpu;
    double interval = (end - start) / sections;
    int size = sections * sizeof(double);
    cudaMalloc((void **)&solution_gpu, size);
    solution = (double *)malloc(size);
    sum_pi<<<(sections + THREADS - 1) / THREADS, THREADS>>>(start, interval, solution_gpu, sections);
    cudaMemcpy(solution, solution_gpu, size, cudaMemcpyDeviceToHost);
    double reduction = 0;
    for (int i = 0; i < sections; i++) {
        reduction += solution[i];
    }
    free(solution);
    cudaFree(solution_gpu);
    return 4 * reduction * interval * 0.5;
}

long seq_pi_print(double start, double end, int sections) {
    auto time_start = std::chrono::steady_clock::now();
    double solution = seq_pi_solve(start, end, sections);
    auto time_end = std::chrono::steady_clock::now();
    printf(
            "Wartosc calki: %.20f, Blad: %.5f%\n",
            solution, (abs(3.1415926535 - solution) / 3.1415926535) * 100
    );
    return std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_start).count();
}

long cuda_pi_print(double start, double end, int sections) {
    auto time_start = std::chrono::steady_clock::now();
    double solution = cuda_pi_solve(start, end, sections);
    auto time_end = std::chrono::steady_clock::now();
    printf(
            "Wartosc calki: %.20f, Blad: %.5f%\n",
            solution, (abs(3.1415926535 - solution) / 3.1415926535) * 100
    );
    return std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_start).count();
}

#endif //PRIR_SEC_INTEGRAL_H