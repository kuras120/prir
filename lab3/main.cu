//
// Created by wojciech on 23.05.2021.
//

#include "source/headers/gpu_info.cuh"
#include "source/headers/division_comparison/divided.cuh"
#include "source/headers/division_comparison/not_divided.cuh"
#include "source/headers/vec_frag_sum.cuh"
#include "source/headers/pi_integral.cuh"
#include "source/headers/euler_gamma.cuh"

int main() {
//    gpu_info();
//    divided();
//    not_divided();
//    vec_frag_sum();
    long seq_pi = 0, seq_euler = 0, cuda_pi = 0, cuda_euler = 0;
    for (int i = 0; i < 10; i++) {
        seq_pi += seq_pi_print(0, 1, 100);
        cuda_pi += cuda_pi_print(0, 1, 100);
        seq_euler += seq_euler_print(100);
        cuda_euler += cuda_euler_print(100);
    }
    printf(
            "Srednie czasy:\nSEQ_PI: %ld\nCUDA_PI: %ld\nSEQ_EULER: %ld\nCUDA_EULER: %ld\n",
            seq_pi / 10, cuda_pi / 10, seq_euler / 10, cuda_euler / 10
    );
    return 0;
}
