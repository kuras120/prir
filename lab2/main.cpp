//
// Created by wojciech on 22.04.2021.
//
#include "source/przyklad.c"
#include "source/headers/ring_propagation.h"
#include "source/headers/number_bcast.h"
#include "source/headers/arrays_sum.h"
#include "source/headers/pi_number.h"

int main(int argc, char **argv) {
    // TODO NALEZY ODKOMENTOWAC TYLKO JEDNA FUNKCJE!
//    przyklad(argc, argv);
//    ring_propagation(argc, argv);
//    number_bcast(argc, argv);
//    arrays_sum(argc, argv, 1);
    pi_number(argc, argv, 500000000, 1);
    return 0;
}