//
// Created by wojciech on 22.04.2021.
//

#ifndef PRIR_RING_PROPAGATION_H
#define PRIR_RING_PROPAGATION_H

#include <iostream>
#include <mpich/mpi.h>
#include "mpi_helper.h"

void ring_propagation(int argc, char **argv) {
    int rank, size, dest, data = 0, number = 0, len = 20;
    char name[20];

    MPI_Status status;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(name, &len);

    do {
        if(rank == 0) {
            // WCZYTANIE WARTOSCI
            printf("Podaj liczbę: \n");
            std::cin >> number;
            dest = rank + 1;
            // WYSLANIE I OCZEKIWANIE NA ODEBRANIE OD OSTATNIEGO PROCESU MINOR
            MPI_Send(&number, 1, MPI_INT, dest, rank, MPI_COMM_WORLD);
            print_mpi("%s: Komunikacja %d -> %d\n", name, {number, rank, dest});
            MPI_Recv(&data, 1, MPI_INT, size - 1, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            print_mpi("%s: Komunikacja %d <- %d, Dane: %d\n", name, {data, rank, status.MPI_SOURCE, data});
        } else {
            // ODEBRANIE I WYSLANIE DO KOLEJNEGO PROCESU Z PIERSCIENIA
            MPI_Recv(&data, 1, MPI_INT, rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            print_mpi("%s: Komunikacja %d <- %d, Dane: %d\n", name, {data, rank, status.MPI_SOURCE, data});
            dest = rank + 1 >= size ? 0 : rank + 1;
            MPI_Send(&data, 1, MPI_INT, dest, rank, MPI_COMM_WORLD);
            print_mpi("%s: Komunikacja %d -> %d\n", name, {data, rank, dest});
        }
    } while (number >= 0 && data >= 0);
    MPI_Finalize();
}

#endif //PRIR_RING_PROPAGATION_H
