//
// Created by wojciech on 23.04.2021.
//

#ifndef PRIR_MPI_HELPER_H
#define PRIR_MPI_HELPER_H

#include <vector>

void print_mpi(const std::string& text, const std::string& name, std::vector<int> data) {
    data.resize(4);
    if (data[0] >= 0) {
        printf(text.c_str(), name.c_str(), data[1], data[2], data[3]);
    }
}

#endif //PRIR_MPI_HELPER_H
