//
// Created by wojciech on 23.04.2021.
//

#ifndef PRIR_ARRAYS_SUM_H
#define PRIR_ARRAYS_SUM_H

#include <random>
#include <cmath>
#include "mpi_helper.h"

void arrays_sum(int argc, char **argv, int debug) {
    srand(time(nullptr));
    int rank, size, sum, check_sum, array_size = 10000, len = 20;
    std::vector<std::vector<int>> source;
    std::vector<int> dest, final_dest;
    char name[20];
    double start, end;

    MPI_Status status;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(name, &len);

    int division = (int)(array_size / (size - 1));
    int last_element = array_size - ((size - 2) * division);
    if (rank == 0) {
        // GENEROWANIE SKLADNIKOW
        check_sum = 0;
        int counter = division;
        start = MPI_Wtime();
        for (int i = 0; i < size - 1; i ++) {
            source.emplace_back();
            if (i == size - 2) counter = last_element;
            for (int j = 0; j < counter; j ++) {
                source[i].push_back(rand() % 101);
                check_sum += source[i][j];
            }
        }
        end = MPI_Wtime();
        printf("Czas sekwencyjny %f\n", end - start);
        print_mpi("%s: Own sum: %d\n", name, {debug, check_sum});
        start = MPI_Wtime();
        // WYSLANIE SKLADNIKOW
        for (int i = 1; i < size - 1; i ++) {
            MPI_Send(source[i - 1].data(), division, MPI_INT, i, i, MPI_COMM_WORLD);
        }
        MPI_Send(source[size - 2].data(), last_element, MPI_INT, size - 1, size - 1, MPI_COMM_WORLD);
        print_mpi("%s: Komunikacja %d -> ALL\n", name, {debug, rank});
    } else {
        // ODEBRANIE SKLADNIKOW I OBLICZENIE SUMY CZESCIOWEJ
        int count = rank == size - 1 ? last_element : division;
        dest.resize(count);
        MPI_Recv(dest.data(), count, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        sum = 0;
        for (int number : dest) {
            sum += number;
        }
        print_mpi("%s: Komunikacja %d <- %d, Sum: %d\n", name, {debug, rank, 0, sum});
    }
    final_dest.resize(size);
    // ODESLANIE SUM CZESCIOWYCH
    MPI_Gather(&sum, 1, MPI_INT, final_dest.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        end = MPI_Wtime();
        printf("Czas zwrownoleglony %f\n", end - start);
        // WYLICZENIE SUMY WLASCIWEJ
        int final_sum = 0;
        final_dest.erase(final_dest.begin());
        for (int element : final_dest) {
            final_sum += element;
        }
        std::string txt = final_sum == check_sum ? "prawidlowa" : "nieprawidlowa";
        print_mpi("Wartosc %s dla finalnej sumy %d\n", txt,{debug, final_sum});
    }
    MPI_Finalize();
}

#endif //PRIR_ARRAYS_SUM_H
