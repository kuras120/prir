//
// Created by wojciech on 23.04.2021.
//

#ifndef PRIR_NUMBER_BCAST_H
#define PRIR_NUMBER_BCAST_H

#include <unistd.h>

void number_bcast(int argc, char **argv) {
    int rank, size, number = 0, len = 20;
    char name[20];

    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(name, &len);

    std::vector<int> dummies(size);
    do {
        if(rank == 0) {
            // WCZYTANIE WARTOSCI
            printf("Podaj liczbę: \n");
            std::cin >> number;
        }
        // WYSLANIE WARTOSCI DO WSZYSTKICH PROCESOW
        MPI_Bcast(&number, 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (rank == 0) {
            print_mpi("%s: Komunikacja %d -> ALL\n", name, {number, rank});
        } else {
            print_mpi("%s: Komunikacja %d <- %d, Dane: %d\n", name, {number, rank, 0, number});
        }
        // ODEBRANIE WSZYSTKICH WARTOSCI W CELU POTWIERDZENIA ZAKONCZENIA PRZETWARZANIA
        MPI_Gather(&number, 1, MPI_INT, dummies.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (rank == 0) {
            print_mpi("%s: Komunikacja %d <- ALL, Dane: ", name, {number, rank});
            for (int number : dummies) {
                print_mpi("%s%d ", "", {number, number});
            }
            printf("\n");
        }
    } while (number >= 0);
    MPI_Finalize();
}

#endif //PRIR_NUMBER_BCAST_H
