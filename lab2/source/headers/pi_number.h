//
// Created by wojciech on 25.04.2021.
//

#ifndef PRIR_PI_NUMBER_H
#define PRIR_PI_NUMBER_H

#include <vector>
#include <cstdlib>
#include "mpi_helper.h"

void pi_number(int argc, char **argv, int components, int debug) {
    int rank, size, array_size = components, len = 20;
    std::vector<std::vector<int>> source;
    std::vector<int> dest;
    std::vector<long double> final_dest;
    char name[20];
    long double sum, check_sum, start, end;

    MPI_Status status;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(name, &len);

    int division = (int)(array_size / (size - 1));
    int last_element = array_size - ((size - 2) * division);
    if (rank == 0) {
        // GENEROWANIE SKLADNIKOW
        check_sum = 0.0;
        int number = 1, counter = division;
        for (int i = 0; i < size - 1; i ++) {
            source.emplace_back();
            if (i == size - 2) counter = last_element;
            for (int j = 0; j < counter; j ++) {
                source[i].push_back(number);
                check_sum += 1.0/number;
                if (number > 0) number = -(number + 2);
                else number = -number + 2;
            }
        }
        check_sum = 4*check_sum;
        printf("%s: Own sum: %Lf\n", name, check_sum);
        start = MPI_Wtime();
        // WYSLANIE SKLADNIKOW
        for (int i = 1; i < size - 1; i ++) {
            MPI_Send(source[i - 1].data(), division, MPI_INT, i, i, MPI_COMM_WORLD);
        }
        MPI_Send(source[size - 2].data(), last_element, MPI_INT, size - 1, size - 1, MPI_COMM_WORLD);
        print_mpi("%s: Komunikacja %d -> ALL\n", name, {debug, rank});
    } else {
        // ODEBRANIE SKLADNIKOW I OBLICZENIE SUMY CZESCIOWEJ
        int count = rank == size - 1 ? last_element : division;
        dest.resize(count);
        MPI_Recv(dest.data(), count, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        sum = 0.0;
        for (int number : dest) {
            sum += 1.0/number;
        }
        printf("%s: Komunikacja %d <- %d, Sum: %.50Lf\n", name, rank, 0, sum);
    }
    final_dest.resize(size);
    // ODESLANIE SUM CZESCIOWYCH
    MPI_Gather(&sum, 1, MPI_LONG_DOUBLE, final_dest.data(), 1, MPI_LONG_DOUBLE, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        end = MPI_Wtime();
        printf("Czas zrownoleglony %Lf\n", end - start);
        // WYLICZENIE SUMY WLASCIWEJ
        long double final_sum = 0.0;
        final_dest.erase(final_dest.begin());
        for (long double element : final_dest) {
            final_sum += element;
        }
        final_sum = 4*final_sum;
        printf("Stare: %.50Lf Nowe %.50Lf\n", check_sum, final_sum);
        std::string txt = final_sum == check_sum ? "prawidlowa" : "nieprawidlowa";
        printf("Wartosc %s dla finalnej sumy %.50Lf\n", txt.c_str(), final_sum);
    }
    MPI_Finalize();
}

#endif //PRIR_PI_NUMBER_H
