//
// Created by wojciech on 21.03.2021.
//

#include "source/petle.c"
#include "source/petle1.c"
#include "source/petle2.c"
#include "source/private.c"
#include "source/headers/matrix_solver.h"

void matrices_times() {
    std::vector<double> times = {0, 0, 0};
    double start, end;
    for (int i = 0; i < 10; i++) {
        start = omp_get_wtime();
        MatrixSolver::solve(250, 250, 250, false);
        end = omp_get_wtime();
        times[0] += end - start;
        start = omp_get_wtime();
        MatrixSolver::solve(500, 500, 500, false);
        end = omp_get_wtime();
        times[1] += end - start;
        start = omp_get_wtime();
        MatrixSolver::solve(750, 750, 750, false);
        end = omp_get_wtime();
        times[2] += end - start;
    }
    printf("Times:\n Matrix250: %f\nMatrix500: %f\nMatrix750: %f\n", times[0]/10, times[1]/10, times[2]/10);
}

int main () {
// Zad1-3
//    loops();
// Zad4
//    loops1();
// Zad5
    loops2();
// Zad6
//    private_built_in();
// Zad7
//    MatrixSolver::solve(4, 2, 3, true);
// Zad8-9
//    matrices_times();
    return 0;
}
