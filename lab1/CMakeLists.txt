set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "-Wall -pedantic -Wextra -fopenmp")
enable_language(CXX)
add_executable(lab1 main.cpp source/petle.c source/petle1.c source/petle2.c source/private.c source/matrix_solver.cpp source/headers/matrix_solver.h)