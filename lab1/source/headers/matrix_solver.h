//
// Created by wojciech on 21.03.2021.
//

#ifndef PRIR_MATRIX_SOLVER_H
#define PRIR_MATRIX_SOLVER_H

#include <cstdio>
#include <omp.h>
#include <vector>
#include <cstdlib>
#include <bits/stdc++.h>

// Zad7 - mnozenie macierzy
class MatrixSolver {
private:
    // Losowanie wartosci do macierzy
    static long random_number();
    // Wypisanie macierzy
    static void printMatrix(const std::vector<std::vector<long>>& matrix, const std::string& matrixName);
    // Generowanie macierzy
    static std::vector<std::vector<long>> generate_matrix(long rows, long columns, bool zeros);
    // Mnozenie macierzy
    static std::vector<std::vector<long>> multiply(std::vector<std::vector<long>> matrix1, std::vector<std::vector<long>> matrix2);
public:
    // Glowna metoda
    static void solve(long N, long M, long P, bool printIt);
};

#endif //PRIR_MATRIX_SOLVER_H
