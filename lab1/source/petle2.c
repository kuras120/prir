//
// Created by wojciech on 19.03.2021.
//

#include <stdio.h>
#include <omp.h>

// Zad5 - test klauzuli reduction dla dodawania
int loops2() {
    omp_set_num_threads(4);
    int size = 5000;
    double start, end;
    int sum, tmp, number = 5;

    printf("With reduction\n");
    sum = 0;
    start = omp_get_wtime();
    // Ustawiony dodatkowy parametr dla zmiennej sum
    #pragma omp parallel default(none) shared(size, number, tmp) reduction(+ : sum)
    {
        // Wykonanie petli for z klauzula reduction - gwarantuje synchroniczna operacje do zmiennej przez wiele watkow
        #pragma omp for
        for (int i = 0; i < size; i++) {
            sum += number*number;
        }
    }
    end = omp_get_wtime();
    printf("Work took %f seconds\nResult %d\n\n", end - start, sum);
    printf("Without reduction\n");
    sum = 0;
    start = omp_get_wtime();
    #pragma omp parallel default(none) shared(size, number, tmp, sum)
    {
        // Wykonanie petli for bez klauzuli reduction - nie gwarantuje synchronicznej oparcji, mozliwe nadpisanie zmiennej
        #pragma omp for
        for (int i = 0; i < size; i++) {
            tmp = number*number;
            sum += tmp;
        }
    }
    end = omp_get_wtime();
    printf("Work took %f seconds\nResult %d\n\n", end - start, sum);
    return 0;
}
