//
// Created by wojciech on 14.03.2021.
//

#include <stdio.h>
#include <omp.h>

//Zad1/2/3 - strategie przydzialu iteracji watkom
int loops() {
    // Udostepniona liczba watkow do przetwarzania
    omp_set_num_threads(4);
    int chunk_size = 3;
    int size = 15;

    printf("Przydzial static, chunk 3\n");
    // Wlaczenie zrownoleglenia kodu, zmienne wspoldzielone: wielkosc chunka, ilosc iteracji
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int my_id = omp_get_thread_num();
        // Kolejka dla watkow petli for
        #pragma omp for schedule(static, chunk_size)
        for (int i = 0; i < size; i++) {
            printf("thread %d, index %d\n", my_id, i);
        }
    }

    printf("\nPrzydzial static, auto chunk\n");
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int my_id = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (int i = 0; i < size; i++) {
            printf("thread %d, index %d\n", my_id, i);
        }
    }

    printf("\nPrzydzial dynamic, chunk 3\n");
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int my_id = omp_get_thread_num();
        #pragma omp for schedule(dynamic, chunk_size)
        for (int i = 0; i < size; i++) {
            printf("thread %d, index %d\n", my_id, i);
        }
    }

    printf("\nPrzydzial dynamic, auto chunk\n");
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int my_id = omp_get_thread_num();
        #pragma omp for schedule(dynamic)
        for (int i = 0; i < size; i++) {
            printf("thread %d, index %d\n", my_id, i);
        }
    }
    return 0;
}
