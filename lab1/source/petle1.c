//
// Created by wojciech on 19.03.2021.
//

#include <stdio.h>
#include <omp.h>

// Zad4 - pomiar czasow dla petle.c z 150 000 iteracji
int loops1() {
    omp_set_num_threads(4);
    int chunk_size = 3;
    int size = 150000;
    double start, end;

    printf("Przydzial static, chunk 3\n");
    start = omp_get_wtime();
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int sum = 0;
        #pragma omp for schedule(static)
        for (int i = 0; i < size; i++) {
            sum += i;
        }
    }
    end = omp_get_wtime();
    printf("Work took %f seconds\n\n", end - start);
    printf("\nPrzydzial static, auto chunk\n");
    start = omp_get_wtime();
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int sum = 0;
        #pragma omp for schedule(static)
        for (int i = 0; i < size; i++) {
            sum += i;
        }
    }
    end = omp_get_wtime();
    printf("Work took %f seconds\n\n", end - start);
    printf("\nPrzydzial dynamic, chunk 3\n");
    start = omp_get_wtime();
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int sum = 0;
        #pragma omp for schedule(static)
        for (int i = 0; i < size; i++) {
            sum += i;
        }
    }
    end = omp_get_wtime();
    printf("Work took %f seconds\n\n", end - start);
    printf("\nPrzydzial dynamic, auto chunk\n");
    start = omp_get_wtime();
    #pragma omp parallel default(none) shared(chunk_size, size)
    {
        int sum = 0;
        #pragma omp for schedule(static)
        for (int i = 0; i < size; i++) {
            sum += i;
        }
    }
    end = omp_get_wtime();
    printf("Work took %f seconds\n\n", end - start);
    return 0;
}
