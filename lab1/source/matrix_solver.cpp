//
// Created by wojciech on 19.03.2021.
//

#include "headers/matrix_solver.h"

long MatrixSolver::random_number() {
    return random() % 9 + 1;
}

std::vector<std::vector<long>> MatrixSolver::generate_matrix(long rows, long columns, bool zeros) {
    std::vector<std::vector<long>> matrix1(rows);
    for (auto i = 0; i < rows; i++) {
        matrix1[i].resize(columns);
        if (zeros) generate(matrix1[i].begin(), matrix1[i].end(), [](){return 0;});
        else generate(matrix1[i].begin(), matrix1[i].end(), random_number);
    }
    return matrix1;
}

std::vector<std::vector<long>> MatrixSolver::multiply(std::vector<std::vector<long>> matrix1, std::vector<std::vector<long>> matrix2) {
    auto result = generate_matrix(matrix1.size(), matrix2[0].size(), true);
    ulong i, j, k, temp;
    #pragma omp parallel for schedule(dynamic) default(none) shared(matrix1, matrix2, result) private(i, j, k, temp) num_threads(4)
    for (i = 0UL; i < matrix1.size(); i++) {
//        #pragma omp parallel for schedule(dynamic) default(none) shared(matrix1, matrix2, result, i) private(j, k, temp) num_threads(4)
        for (j = 0UL; j < matrix2[0].size(); j++) {
            // Szybsze jest wpisanie do zmiennej od wyszukiwania za kazdym razem miejsca w vectorze
            temp = 0;
            for (k = 0UL; k < matrix2.size(); k++) {
                temp += matrix1[i][k] * matrix2[k][j];
            }
            result[i][j] = temp;
        }
    }
    printf("Multiplication has been completed for size: %zu\n", matrix1.size());
    return result;
}

void MatrixSolver::solve(long N, long M, long P, bool printIt) {
    srand(time(nullptr));
    auto matrix1 = generate_matrix(N, M, false);
    auto matrix2 = generate_matrix(M, P, false);
    auto result = multiply(matrix1, matrix2);
    if (printIt) {
        printMatrix(matrix1, "Matrix1");
        printMatrix(matrix2, "Matrix2");
        printMatrix(result, "Result");
    }
}

void MatrixSolver::printMatrix(const std::vector<std::vector<long>>& matrix, const std::string& matrixName) {
    printf("%s column size %zu, row size %zu\n", matrixName.c_str(), matrix.size(), matrix[0].size());
    for (const auto& row : matrix) {
        for (auto element : row) {
            printf("%zu  ", element);
        }
        printf("\n");
    }
    printf("\n\n");
}
